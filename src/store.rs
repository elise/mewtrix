use sqlx::{FromRow, SqlitePool};
#[derive(Clone, Debug, FromRow)]
pub struct DeviceStore {
    pub matrix_id: String,
    pub access_token: String,
    pub device_id: String,
}

impl DeviceStore {
    pub async fn all(pool: &SqlitePool) -> sqlx::Result<Vec<Self>> {
        sqlx::query_as!(
            Self,
            "SELECT matrix_id, access_token, device_id FROM device_store"
        )
        .fetch_all(pool)
        .await
    }

    pub async fn by_matrix_id(pool: &SqlitePool, matrix_id: &str) -> sqlx::Result<Option<Self>> {
        sqlx::query_as!(
            Self,
            "SELECT matrix_id, access_token, device_id FROM device_store WHERE matrix_id = ?",
            matrix_id
        )
        .fetch_optional(pool)
        .await
    }

    pub async fn upsert(&self, pool: &SqlitePool) -> sqlx::Result<()> {
        sqlx::query!("INSERT INTO device_store (matrix_id, access_token, device_id) VALUES (?, ?, ?) ON CONFLICT(matrix_id) DO UPDATE SET access_token = ?2, device_id = ?3", self.matrix_id, self.access_token, self.device_id).execute(pool).await?;
        Ok(())
    }

    pub async fn delete_by_matrix_id(pool: &SqlitePool, matrix_id: &str) -> sqlx::Result<()> {
        sqlx::query!("DELETE FROM device_store WHERE matrix_id = ?", matrix_id)
            .execute(pool)
            .await?;
        Ok(())
    }
}
