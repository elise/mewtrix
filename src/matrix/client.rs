use reqwest::{header::AUTHORIZATION, Client, Response, StatusCode, Url};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use sqlx::SqlitePool;
use std::time::Duration;

use thiserror::Error;

use crate::store::DeviceStore;

#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub struct Empty {}

use super::{
    api::discovery::{BaseUrl, DiscoveryInformation},
    error::{MatrixErrorCode, MatrixErrorResponse},
    identifier::UserId,
};

pub struct RequestClient {
    client: reqwest::Client,
}

#[derive(Debug, Error)]
pub enum RequestClientError {
    #[error("API Error: ({0}) {1:?}")]
    MatrixError(StatusCode, MatrixErrorResponse),
    #[error("Reqwest Error: {0}")]
    ReqwestError(#[from] reqwest::Error),
    #[error("Parse Error: {0}")]
    ParseError(StatusCode),
}

impl RequestClient {
    pub fn new() -> Result<Self, reqwest::Error> {
        Ok(Self {
            client: Client::builder()
                .timeout(Duration::from_secs(60))
                .user_agent("mewtrix/0.1.0")
                .build()?,
        })
    }

    async fn parse_response<Output: DeserializeOwned>(
        resp: Response,
    ) -> Result<Output, RequestClientError> {
        let status = resp.status();
        if status == StatusCode::OK {
            match resp.json().await {
                Ok(out) => return Ok(out),
                Err(why) => log::error!("Parse Error: {:?}", why),
            }
        } else {
            match resp.json().await {
                Ok(err) => return Err(RequestClientError::MatrixError(status, err)),
                Err(why) => log::error!("Parse Error: {:?}", why),
            }
        }

        Err(RequestClientError::ParseError(status))
    }

    pub async fn get<Input: Serialize + std::fmt::Debug, Output: DeserializeOwned>(
        &self,
        url: Url,
        access_token: Option<String>,
        body: Option<&Input>,
    ) -> Result<Output, RequestClientError> {
        log::info!("Making GET request to {}, body: {:?}", url, body);
        let mut req = self.client.get(url);

        if let Some(access_token) = access_token {
            req = req.header(AUTHORIZATION, format!("Bearer {}", access_token));
        }

        if let Some(body) = body {
            req = req.json(body);
        }

        let resp = req.send().await?;
        Self::parse_response(resp).await
    }

    pub async fn post<Input: Serialize + std::fmt::Debug, Output: DeserializeOwned>(
        &self,
        url: Url,
        access_token: Option<String>,
        body: Option<&Input>,
    ) -> Result<Output, RequestClientError> {
        log::info!("Making POST request to {}, body: {:?}", url, body);
        let mut req = self.client.post(url);

        if let Some(access_token) = access_token {
            req = req.header(AUTHORIZATION, format!("Bearer {}", access_token));
        }

        if let Some(body) = body {
            req = req.json(body);
        }

        let resp = req.send().await?;
        Self::parse_response(resp).await
    }

    pub async fn put<Input: Serialize + std::fmt::Debug, Output: DeserializeOwned>(
        &self,
        url: Url,
        access_token: Option<String>,
        body: Option<&Input>,
    ) -> Result<Output, RequestClientError> {
        log::info!("Making PUT request to {}, body: {:?}", url, body);
        let mut req = self.client.put(url);

        if let Some(access_token) = access_token {
            req = req.header(AUTHORIZATION, format!("Bearer {}", access_token));
        }

        if let Some(body) = body {
            req = req.json(body);
        }

        let resp = req.send().await?;
        Self::parse_response(resp).await
    }
}

#[derive(Debug, Error)]
pub enum MatrixClientError {
    #[error("Request Error: {0}")]
    ApiError(RequestClientError),
    #[error("Invalid Auth")]
    InvalidAuthToken { soft_logout: bool },
    #[error("Missing Auth")]
    MissingAuthToken,
    #[error("Not Found")]
    NotFound,
    #[error("DB Error: {0:?}")]
    SqlxError(#[from] sqlx::Error),
}

impl From<RequestClientError> for MatrixClientError {
    fn from(err: RequestClientError) -> Self {
        if let RequestClientError::MatrixError(status, err) = &err {
            if status == &StatusCode::UNAUTHORIZED {
                if err.errcode == MatrixErrorCode::UNKNOWN_TOKEN {
                    let soft_logout = err
                        .extras
                        .get("soft_logout")
                        .unwrap()
                        .as_bool()
                        .unwrap_or_default();
                    return MatrixClientError::InvalidAuthToken { soft_logout };
                } else if err.errcode == MatrixErrorCode::MISSING_TOKEN {
                    return MatrixClientError::MissingAuthToken;
                }
            } else if status == &StatusCode::NOT_FOUND {
                return MatrixClientError::NotFound;
            }
        }

        MatrixClientError::ApiError(err)
    }
}

pub struct MatrixClient {
    pub(crate) api_client: RequestClient,
    pub(crate) homeserver: BaseUrl,
    pub(crate) user_id: UserId,
    pub(crate) pool: sqlx::SqlitePool,
    pub since: Option<String>
}

impl MatrixClient {
    pub async fn new(
        pool: SqlitePool,
        homeserver: Url,
        user_id: UserId,
    ) -> Result<Self, MatrixClientError> {
        let mut client = Self {
            api_client: RequestClient::new().expect("Failed to initialize reqwest client"),
            homeserver: BaseUrl {
                base_url: homeserver,
            },
            user_id,
            pool,
            since: None,
        };

        match client.discovery_information().await {
            Ok(discovery) => {
                client.reconfigure(discovery);
                Ok(client)
            }
            Err(MatrixClientError::NotFound) => Ok(client),
            Err(err) => Err(err),
        }
    }

    pub fn homeserver_url(&self, path: &str) -> Url {
        let mut new_url = self.homeserver.base_url.clone();
        new_url.set_path(path);
        new_url
    }

    pub fn reconfigure(&mut self, info: DiscoveryInformation) {
        log::info!("Client reconfiguring to {:?}", info);
        self.homeserver = info.homeserver;
    }

    pub async fn access_token(&self) -> Result<Option<String>, MatrixClientError> {
        Ok(
            DeviceStore::by_matrix_id(&self.pool, self.user_id.to_string().as_str())
                .await?
                .map(|store| store.access_token),
        )
    }
}
