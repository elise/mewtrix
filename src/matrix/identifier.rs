#![allow(dead_code)]

use std::{convert::TryFrom, fmt::Display, hash::Hash, str::FromStr};

/// MXID variants ([spec](https://matrix.org/docs/spec/appendices#common-identifier-format))
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum IdType {
    /// Identifies a user
    UserId,
    /// Identifies a room
    RoomId,
    /// Identifies an event
    EventId,
    /// Identifies a group
    GroupId,
    /// Identifies a room via an alias in the form `#room_alias:domain`
    RoomAlias,
}

impl IdType {
    /// Returns the type sigil
    pub fn to_sigil(&self) -> &str {
        match self {
            IdType::UserId => "@",
            IdType::RoomId => "!",
            IdType::EventId => "$",
            IdType::GroupId => "+",
            IdType::RoomAlias => "#",
        }
    }
}

impl Display for IdType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self.to_sigil()))
    }
}

#[derive(Clone, Debug)]
pub enum MxidParseError {
    InvalidSigil,
}

impl Display for MxidParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{:?}", self))
    }
}

impl FromStr for IdType {
    type Err = MxidParseError;

    /// Parses Sigils into an MXID type
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.chars().next() {
            Some('@') => Ok(Self::UserId),
            Some('!') => Ok(Self::RoomId),
            Some('$') => Ok(Self::EventId),
            Some('+') => Ok(Self::GroupId),
            Some('#') => Ok(Self::RoomAlias),
            _ => Err(MxidParseError::InvalidSigil),
        }
    }
}

/// Represents an MXID, has a type and a body
#[derive(Clone, Debug, Eq)]
pub struct MatrixId {
    /// Type of Matrix ID
    pub id_type: IdType,
    /// Contents of Matrix ID
    pub body: String,
}

impl PartialEq for MatrixId {
    fn eq(&self, other: &Self) -> bool {
        self.id_type == other.id_type && self.body == other.body
    }
}

impl Hash for MatrixId {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.to_string().hash(state)
    }
}

impl MatrixId {
    /// Creates a new Matrix ID
    pub fn new(id_type: IdType, body: String) -> Self {
        Self { id_type, body }
    }

    /// Helper function that calls `Self::new(IdType::UserId, body)`
    pub fn full_user(body: String) -> Self {
        Self::new(IdType::UserId, body)
    }

    /// Turns ("username", "homeserver.tld") into `@username:homeserver.tld`
    pub fn user(username: &str, homeserver: &str) -> Self {
        Self::full_user(format!("{}:{}", username, homeserver))
    }
}

impl Display for MatrixId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}{}", self.id_type.to_sigil(), self.body))
    }
}

impl FromStr for MatrixId {
    type Err = MxidParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let id_type = IdType::from_str(s)?;
        let body = s[id_type.to_sigil().len()..s.len()].to_string();
        Ok(Self { id_type, body })
    }
}

#[derive(Clone, Debug)]
pub enum IdParseError {
    MissingSeperator,
    MissingSigil,
}

impl Display for IdParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{:?}", self))
    }
}

#[derive(Clone, Debug, Eq)]
pub struct EventId {
    pub id: String,
}

impl PartialEq for EventId {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Hash for EventId {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.to_string().hash(state)
    }
}

impl Display for EventId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("${}", self.id))
    }
}

impl FromStr for EventId {
    type Err = IdParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(sigil) = s.chars().next() {
            if sigil == '$' {
                return Ok(Self {
                    id: s[1..].to_string(),
                });
            }
        }
        Err(IdParseError::MissingSigil)
    }
}

#[derive(Clone, Debug, Eq)]
pub struct UserId {
    pub user: String,
    pub homeserver: String,
}

impl PartialEq for UserId {
    fn eq(&self, other: &Self) -> bool {
        self.user == other.user && self.homeserver == other.homeserver
    }
}

impl Hash for UserId {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.to_string().hash(state)
    }
}

impl Display for UserId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("@{}:{}", self.user, self.homeserver))
    }
}

impl FromStr for UserId {
    type Err = IdParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(sigil) = s.chars().next() {
            if sigil == '@' {
                return match (&s[1..s.len()]).split_once(':') {
                    Some((user, homeserver)) => Ok(Self {
                        user: user.to_string(),
                        homeserver: homeserver.to_string(),
                    }),
                    None => Err(IdParseError::MissingSeperator),
                };
            }
        }
        Err(IdParseError::MissingSigil)
    }
}

#[derive(Clone, Debug, Eq)]
pub struct RoomId {
    pub id: String,
    pub homeserver: String,
}

impl Ord for RoomId {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.homeserver == other.homeserver {
            true => self.id.cmp(&other.id),
            false => self.homeserver.cmp(&other.homeserver)
        }
    }
}

impl PartialOrd for RoomId {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match self.homeserver == other.homeserver {
            true => self.id.partial_cmp(&other.id),
            false => self.homeserver.partial_cmp(&other.homeserver)
        }
    }
}

impl PartialEq for RoomId {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id && self.homeserver == other.homeserver
    }
}

impl Hash for RoomId {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.to_string().hash(state)
    }
}

impl Display for RoomId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("!{}:{}", self.id, self.homeserver))
    }
}

impl FromStr for RoomId {
    type Err = IdParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(sigil) = s.chars().next() {
            if sigil == '!' {
                return match (&s[1..s.len()]).split_once(':') {
                    Some((id, homeserver)) => Ok(Self {
                        id: id.to_string(),
                        homeserver: homeserver.to_string(),
                    }),
                    None => Err(IdParseError::MissingSeperator),
                };
            }
        }
        Err(IdParseError::MissingSigil)
    }
}

#[derive(Debug)]
pub enum MxidTryIntoError {
    InvalidType(IdType),
    MissingSeperator,
}

impl TryFrom<&MatrixId> for UserId {
    type Error = MxidTryIntoError;

    fn try_from(value: &MatrixId) -> Result<Self, Self::Error> {
        match value.id_type.clone() {
            IdType::UserId => match value.body.split_once(':') {
                Some((user, homeserver)) => Ok(Self {
                    user: user.to_string(),
                    homeserver: homeserver.to_string(),
                }),
                None => Err(MxidTryIntoError::MissingSeperator),
            },
            id_type => Err(MxidTryIntoError::InvalidType(id_type)),
        }
    }
}

impl From<&UserId> for MatrixId {
    fn from(user_id: &UserId) -> Self {
        Self {
            id_type: IdType::UserId,
            body: format!("{}:{}", user_id.user, user_id.homeserver),
        }
    }
}
