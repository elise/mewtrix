use super::api::events::SyncResponse;
use async_trait::async_trait;

#[async_trait]
pub trait SyncHandler {
    async fn sync(&mut self, sync: SyncResponse);
}
