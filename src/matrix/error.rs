use std::{borrow::Cow, fmt::Display, str::FromStr};

use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};
use serde_with::{serde_as, DisplayFromStr};

#[derive(Debug, PartialEq)]
pub enum MatrixErrorCodeParseError {
    MissingSeperator,
}

impl Display for MatrixErrorCodeParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{:?}", self))
    }
}

#[derive(Debug, PartialEq)]
pub struct MatrixErrorCode {
    pub namespace: Cow<'static, str>,
    pub code: Cow<'static, str>,
}

impl MatrixErrorCode {
    pub const MISSING_TOKEN: Self = Self {
        namespace: Cow::Borrowed("M"),
        code: Cow::Borrowed("MISSING_TOKEN"),
    };
    pub const UNKNOWN_TOKEN: Self = Self {
        namespace: Cow::Borrowed("M"),
        code: Cow::Borrowed("UNKNOWN_TOKEN"),
    };
}

impl Display for MatrixErrorCode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}_{}", self.namespace, self.code))
    }
}

impl FromStr for MatrixErrorCode {
    type Err = MatrixErrorCodeParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.split_once('_') {
            Some((namespace, code)) => Ok(Self {
                namespace: Cow::Owned(namespace.to_string()),
                code: Cow::Owned(code.to_string()),
            }),
            None => Err(MatrixErrorCodeParseError::MissingSeperator),
        }
    }
}

#[serde_as]
#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct MatrixErrorResponse {
    #[serde_as(as = "DisplayFromStr")]
    pub errcode: MatrixErrorCode,
    pub error: String,
    #[serde(flatten)]
    pub extras: Map<String, Value>,
}

impl From<&str> for MatrixErrorResponse {
    fn from(code: &str) -> Self {
        Self {
            errcode: MatrixErrorCode::from_str(code).unwrap(),
            error: String::new(),
            extras: Map::new(),
        }
    }
}
