pub mod room;

use std::collections::HashMap;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use serde_with::{serde_as, skip_serializing_none, DisplayFromStr, TimestampMilliSeconds};

use super::identifier::{EventId, MatrixId, RoomId, UserId};

#[skip_serializing_none]
#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
pub struct RoomEvent {
    #[serde(flatten)]
    pub base: Event,
    #[serde_as(as = "DisplayFromStr")]
    pub event_id: EventId,
    #[serde_as(as = "DisplayFromStr")]
    pub sender: UserId,
    #[serde_as(as = "TimestampMilliSeconds")]
    pub origin_server_ts: DateTime<Utc>,
    /// Only `None` on events from `/sync`
    pub room_id: Option<String>,
    pub unsigned: Option<UnsignedData>,
    #[serde_as(as = "Option<DisplayFromStr>")]
    #[serde(default)]
    pub redacts: Option<EventId>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct StateEvent {
    #[serde(flatten)]
    pub base: RoomEvent,
    pub state_key: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prev_content: Option<Event>,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct UnsignedData {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub age: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub redacted_because: Option<Box<Event>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub transaction_id: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum Event {
    Matrix(MatrixEvent),
    Unknown {
        r#type: String,
        content: Value,
    },
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "type", content = "content")]
pub enum MatrixEvent {
    #[serde(rename = "m.room.canonical_alias")]
    RoomCanonicalAlias(room::CanonicalAliasContent),
    #[serde(rename = "m.room.create")]
    RoomCreate(room::CreateContent),
    #[serde(rename = "m.room.join_rules")]
    JoinRules(room::JoinRulesContent),
    #[serde(rename = "m.room.member")]
    Member(room::MemberContent),
    #[serde(rename = "m.room.power_levels")]
    PowerLevels(room::PowerLevelContent),
    #[serde(rename = "m.room.redaction")]
    Redaction(room::RedactionContent),
    #[serde(rename = "m.room.name")]
    Name(room::NameContent),
    #[serde(rename = "m.direct")]
    Direct(DirectContent),
    #[serde(rename = "m.room.message")]
    Message(room::Message),
}


#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
pub struct DirectContent {
    #[serde_as(as = "HashMap<DisplayFromStr, Vec<DisplayFromStr>>")]
    #[serde(flatten)]
    pub hashmap: HashMap<UserId, Vec<RoomId>>,
}

#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
pub struct StrippedState {
    pub state_key: String,
    #[serde_as(as = "DisplayFromStr")]
    pub sender: MatrixId,
    #[serde(flatten)]
    pub event: Event,
}