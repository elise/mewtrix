use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_json::Value;
use serde_with::{serde_as, skip_serializing_none, DisplayFromStr};

use crate::matrix::{
    identifier::UserId,
    room::{JoinRules, MembershipState},
};

use super::UnsignedData;

#[derive(Debug, Deserialize, Serialize)]
pub struct CanonicalAliasContent {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alias: Option<String>,
    #[serde(default)]
    pub alt_aliases: Vec<String>,
}

pub fn true_default() -> bool {
    true
}

pub fn room_version_default() -> String {
    String::from("1")
}

#[derive(Debug, Deserialize, Serialize)]
pub struct CreateContent {
    pub creator: String,
    #[serde(rename = "m.federate", default = "true_default")]
    pub federate: bool,
    #[serde(default = "room_version_default")]
    pub room_version: String,
    pub predecessor: Option<PreviousRoom>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PreviousRoom {
    pub room_id: String,
    pub event_id: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct JoinRulesContent {
    pub join_rule: JoinRules,
}

#[skip_serializing_none]
#[derive(Debug, Deserialize, Serialize)]
pub struct MemberContent {
    pub avatar_url: Option<String>,
    pub displayname: Option<String>,
    pub membership: MembershipState,
    #[serde(default)]
    pub is_direct: bool,
    pub third_party_invite: Option<ThirdPartyInvite>,
    #[serde(default)]
    pub unsigned: UnsignedData,
}

#[skip_serializing_none]
#[derive(Debug, Deserialize, Serialize)]
pub struct ThirdPartyInvite {
    pub display_name: String,
    pub signed: Value,
}

pub fn default_0() -> i64 {
    0
}

pub fn default_50() -> i64 {
    50
}

#[serde_as]
#[skip_serializing_none]
#[derive(Debug, Deserialize, Serialize)]
pub struct PowerLevelContent {
    #[serde(default = "default_50")]
    pub ban: i64,
    #[serde(default)]
    pub events: HashMap<String, i64>,
    #[serde(default = "default_0")]
    pub events_default: i64,
    #[serde(default = "default_50")]
    pub invite: i64,
    #[serde(default = "default_50")]
    pub kick: i64,
    #[serde(default = "default_50")]
    pub redact: i64,
    #[serde(default = "default_50")]
    pub state_default: i64,
    #[serde_as(as = "HashMap<DisplayFromStr, _>")]
    pub users: HashMap<UserId, i64>,
    #[serde(default = "default_0")]
    pub users_default: i64,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct NotificationPowerLevel {
    #[serde(default = "default_50")]
    pub room: i64,
}

#[skip_serializing_none]
#[derive(Debug, Deserialize, Serialize)]
pub struct RedactionContent {
    pub reason: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct NameContent {
    pub name: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(tag = "msgtype")]
pub enum MessageType {
    #[serde(rename = "m.text")]
    Text {
        format: Option<String>,
        formatted_body: Option<String>,
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Message {
    pub body: String,
    #[serde(flatten)]
    pub message: MessageType
}