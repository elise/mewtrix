use crate::matrix::{client::{Empty, MatrixClient, MatrixClientError}, identifier::RoomId};
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};

#[derive(Debug, Deserialize, Serialize)]
pub struct JoinedRoomsResponse {
    pub joined_rooms: Vec<String>,
}

#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
struct RoomIdResponse {
    #[serde_as(as = "DisplayFromStr")]
    pub room_id: RoomId
}

impl MatrixClient {
    pub async fn joined_rooms(&self) -> Result<Vec<String>, MatrixClientError> {
        Ok(self
            .api_client
            .get::<Empty, JoinedRoomsResponse>(
                self.homeserver_url("/_matrix/client/r0/joined_rooms"),
                self.access_token().await?,
                None,
            )
            .await?
            .joined_rooms)
    }

    pub async fn join_room(&self, room_id: RoomId) -> Result<RoomId, MatrixClientError> {
        Ok(self
            .api_client
            .post::<Empty, RoomIdResponse>(
                self.homeserver_url(format!("/_matrix/client/r0/rooms/{}/join", urlencoding::encode(room_id.to_string().as_str())).as_str()),
                self.access_token().await?,
                None,
            )
            .await?
            .room_id)
    }

    pub async fn leave_room(&self, room_id: RoomId) -> Result<(), MatrixClientError> {
        self
            .api_client
            .post::<Empty, Empty>(
                self.homeserver_url(format!("/_matrix/client/r0/rooms/{}/leave", urlencoding::encode(room_id.to_string().as_str())).as_str()),
                self.access_token().await?,
                None,
            )
            .await?;
        Ok(())
    }

    pub async fn forget_room(&self, room_id: RoomId) -> Result<(), MatrixClientError> {
        self
            .api_client
            .post::<Empty, Empty>(
                self.homeserver_url(format!("/_matrix/client/r0/rooms/{}/forget", urlencoding::encode(room_id.to_string().as_str())).as_str()),
                self.access_token().await?,
                None,
            )
            .await?;
        Ok(())
    }
}
