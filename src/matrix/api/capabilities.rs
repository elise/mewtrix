use crate::matrix::client::{Empty, MatrixClient, MatrixClientError};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct ChangePasswordCapability {
    pub enabled: bool,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct RoomVersionsCapability {
    pub default: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Capabilities {
    #[serde(rename = "m.change_password")]
    pub change_password: Option<ChangePasswordCapability>,
    #[serde(rename = "m.room_versions")]
    pub room_versions: Option<RoomVersionsCapability>,
}

#[derive(Debug, Deserialize, Serialize)]
struct CapabilitiesResponse {
    pub capabilities: Capabilities,
}

impl MatrixClient {
    pub async fn capabilities(&self) -> Result<Capabilities, MatrixClientError> {
        Ok(self
            .api_client
            .get::<Empty, CapabilitiesResponse>(
                self.homeserver_url("/_matrix/client/r0/capabilities"),
                self.access_token().await?,
                None,
            )
            .await?
            .capabilities)
    }
}
