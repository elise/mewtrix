use colored::{ColoredString, Colorize};
use enum_utils::IterVariants;
use std::{collections::HashMap, fmt::Display, str::FromStr};

use serde::{Deserialize, Serialize};
use serde_with::{serde_as, skip_serializing_none, DisplayFromStr};

use crate::matrix::{client::{Empty, MatrixClient, MatrixClientError}, events::{Event, MatrixEvent, RoomEvent, StateEvent, StrippedState}, identifier::{EventId, RoomId}, room::RoomSummary};

#[derive(Copy, Clone, Debug, Deserialize, Serialize, sqlx::Type, IterVariants)]
#[serde(rename_all = "lowercase")]
#[sqlx(rename_all = "lowercase")]
pub enum Presence {
    Offline,
    Online,
    Unavailable,
}

impl Presence {
    pub fn colored_string(&self) -> ColoredString {
        match self {
            Presence::Offline => "Offline".blue(),
            Presence::Online => "Online".bright_green(),
            Presence::Unavailable => "Unavailable".bright_red(),
        }
    }

    pub fn variant_list() -> String {
        Presence::iter()
            .map(|x| x.colored_string().to_string())
            .collect::<Vec<String>>()
            .join(", ")
    }
}

impl AsRef<str> for Presence {
    fn as_ref(&self) -> &str {
        match self {
            Presence::Offline => "Offline",
            Presence::Online => "Online",
            Presence::Unavailable => "Unavailable",
        }
    }
}

impl Display for Presence {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.as_ref())
    }
}

#[derive(Debug)]
pub enum PresenceParseError {
    InvalidPresence,
}

impl FromStr for Presence {
    type Err = PresenceParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lower = s.to_lowercase();
        match lower.as_str() {
            "offline" => Ok(Self::Offline),
            "online" => Ok(Self::Online),
            "unavailable" => Ok(Self::Unavailable),
            _ => Err(PresenceParseError::InvalidPresence),
        }
    }
}

impl Default for Presence {
    fn default() -> Self {
        Self::Online
    }
}

#[skip_serializing_none]
#[derive(Debug, Deserialize, Serialize)]
pub struct SyncRequest {
    pub filter: Option<String>,
    pub since: Option<String>,
    #[serde(default)]
    pub full_state: bool,
    pub set_presence: Option<Presence>,
    #[serde(default)]
    pub timeout: u64,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SyncResponse {
    pub next_batch: String,
    #[serde(default)]
    pub rooms: SyncResponseRooms,
    #[serde(default)]
    pub presence: EventContainer,
    #[serde(default)]
    pub account_data: EventContainer,
}

#[serde_as]
#[derive(Debug, Default, Deserialize, Serialize)]
pub struct SyncResponseRooms {
    #[serde(default)]
    #[serde_as(as = "HashMap<DisplayFromStr, _>")]
    pub join: HashMap<RoomId, SyncResponseJoinRoom>,
    #[serde(default)]
    #[serde_as(as = "HashMap<DisplayFromStr, _>")]
    pub invite: HashMap<RoomId, SyncResponseInviteRoom>,
    #[serde(default)]
    #[serde_as(as = "HashMap<DisplayFromStr, _>")]
    pub leave: HashMap<RoomId, SyncResponseLeaveRoom>,
}

#[skip_serializing_none]
#[derive(Debug, Deserialize, Serialize)]
pub struct SyncResponseJoinRoom {
    pub summary: Option<RoomSummary>,
    #[serde(default)]
    pub state: StateEventContainer,
    #[serde(default)]
    pub timeline: Timeline,
    #[serde(default)]
    pub ephemeral: EventContainer,
    #[serde(default)]
    pub account_data: EventContainer,
    #[serde(default)]
    pub unread_notifications: UnreadNotificationCount,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct SyncResponseInviteRoom {
    pub invite_state: StrippedStateEventContainer,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct SyncResponseLeaveRoom {
    pub state: StateEventContainer,
    pub timeline: Timeline,
    pub account_data: EventContainer,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct UnreadNotificationCount {
    pub highlight_count: u64,
    pub notification_count: u64,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct EventContainer {
    #[serde(default)]
    pub events: Vec<Event>,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct StateEventContainer {
    #[serde(default)]
    pub events: Vec<StateEvent>,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct StrippedStateEventContainer {
    #[serde(default)]
    pub events: Vec<StrippedState>,
}

#[skip_serializing_none]
#[derive(Debug, Default, Deserialize, Serialize)]
pub struct Timeline {
    #[serde(default)]
    pub events: Vec<RoomEvent>,
    #[serde(default)]
    pub limited: bool,
    pub prev_batch: Option<String>,
}

impl MatrixClient {
    pub async fn sync(&mut self) -> Result<SyncResponse, MatrixClientError> {
        let res = self
            .api_client
            .get::<_, SyncResponse>(
                self.homeserver_url("/_matrix/client/r0/sync"),
                self.access_token().await?,
                Some(&SyncRequest {
                    filter: None,
                    since: self.since.take(),
                    full_state: false,
                    set_presence: None,
                    timeout: 0,
                }),
            )
            .await?;

        self.since = Some(res.next_batch.clone());
        
        Ok(res)
    }

    pub async fn send_room_event(&self, room_id: RoomId, event: MatrixEvent) -> Result<(), MatrixClientError> {
        let value = serde_json::to_value(&event).unwrap();
        let map = value.as_object().unwrap();
        let event_type = map.get("type").unwrap().as_str().unwrap(); // plsno
        let body = map.get("content").unwrap().as_object().unwrap();
        self.api_client.put::<_, Empty>(self.homeserver_url(format!("/_matrix/client/r0/rooms/{}/send/{}/{}", urlencoding::encode(room_id.to_string().as_str()), event_type, uuid::Uuid::new_v4().to_string()).as_str()), self.access_token().await?, Some(body)).await?;
        Ok(())
    }
    
    pub async fn redact_event(&self, room_id: RoomId, event_id: EventId) -> Result<(), MatrixClientError> {
        self.api_client.put::<Empty, Empty>(self.homeserver_url(format!("/_matrix/client/r0/rooms/{}/redact/{}/{}", urlencoding::encode(room_id.to_string().as_str()), urlencoding::encode(event_id.to_string().as_str()), uuid::Uuid::new_v4().to_string()).as_str()), self.access_token().await?, Some(&Empty{})).await?;
        Ok(())
    }
}
