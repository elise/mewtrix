use crate::matrix::{
    client::{Empty, MatrixClient, MatrixClientError},
    identifier::UserId,
};

use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};

#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
pub struct AvatarUrlContainer {
    #[serde(default)]
    #[serde_as(as = "Option<DisplayFromStr>")]
    pub avatar_url: Option<url::Url>,
}

impl MatrixClient {
    pub async fn avatar_url(
        &self,
        user_id: Option<UserId>,
    ) -> Result<Option<url::Url>, MatrixClientError> {
        let user = user_id.as_ref().unwrap_or(&self.user_id).to_string();
        log::info!("Getting avatar URL of {}", user);
        Ok(self
            .api_client
            .get::<Empty, AvatarUrlContainer>(
                self.homeserver_url(&format!(
                    "/_matrix/client/r0/profile/{}/avatar_url",
                    urlencoding::encode(&user)
                )),
                self.access_token().await?,
                None,
            )
            .await?
            .avatar_url)
    }

    pub async fn set_avatar_url(
        &self,
        avatar_url: Option<url::Url>,
    ) -> Result<(), MatrixClientError> {
        log::info!("Setting avatar URL to {:?}", avatar_url);
        self.api_client
            .put::<AvatarUrlContainer, Empty>(
                self.homeserver_url(&format!(
                    "/_matrix/client/r0/profile/{}/avatar_url",
                    urlencoding::encode(&self.user_id.to_string())
                )),
                self.access_token().await?,
                Some(&AvatarUrlContainer { avatar_url }),
            )
            .await?;
        Ok(())
    }
}
