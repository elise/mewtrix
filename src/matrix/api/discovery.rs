use crate::matrix::client::{Empty, MatrixClient, MatrixClientError};
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DiscoveryInformation {
    #[serde(rename = "m.homeserver")]
    pub homeserver: BaseUrl,
    #[serde(rename = "m.identity_server")]
    pub identity_server: Option<BaseUrl>,
}

#[serde_as]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct BaseUrl {
    #[serde_as(as = "DisplayFromStr")]
    pub base_url: url::Url,
}

impl MatrixClient {
    pub async fn discovery_information(&self) -> Result<DiscoveryInformation, MatrixClientError> {
        Ok(self
            .api_client
            .get::<Empty, _>(
                self.homeserver_url("/.well-known/matrix/client"),
                None,
                None,
            )
            .await?)
    }
}
