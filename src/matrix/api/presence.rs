use crate::matrix::{
    client::{Empty, MatrixClient, MatrixClientError},
    identifier::UserId,
};
use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;

use super::events::Presence;

#[skip_serializing_none]
#[derive(Debug, Deserialize, Serialize)]
pub struct SetStatusRequest {
    pub presence: Presence,
    pub status_msg: Option<String>,
}

#[skip_serializing_none]
#[derive(Debug, Deserialize, Serialize)]
pub struct GetStatusResponse {
    pub presence: Presence,
    pub last_active_ago: Option<i64>,
    pub status_msg: Option<String>,
    pub currently_active: Option<bool>,
}

impl MatrixClient {
    pub async fn set_status(
        &self,
        presence: Presence,
        status_msg: Option<String>,
    ) -> Result<(), MatrixClientError> {
        self.api_client
            .put::<SetStatusRequest, Empty>(
                self.homeserver_url(&format!(
                    "/_matrix/client/r0/presence/{}/status",
                    urlencoding::encode(&self.user_id.to_string())
                )),
                self.access_token().await?,
                Some(&SetStatusRequest {
                    presence,
                    status_msg,
                }),
            )
            .await?;
        Ok(())
    }

    pub async fn status(
        &self,
        user_id: Option<UserId>,
    ) -> Result<GetStatusResponse, MatrixClientError> {
        Ok(self
            .api_client
            .get::<Empty, GetStatusResponse>(
                self.homeserver_url(&format!(
                    "/_matrix/client/r0/presence/{}/status",
                    urlencoding::encode(user_id.as_ref().unwrap_or(&self.user_id).to_string().as_str())
                )),
                self.access_token().await?,
                None,
            )
            .await?)
    }
}
