use std::{convert::Infallible, fmt::Display, str::FromStr};

use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};

#[derive(Debug)]
pub enum PaginationDirection {
    Forwards,
    Backwards,
}

impl Default for PaginationDirection {
    fn default() -> Self {
        Self::Forwards
    }
}

impl FromStr for PaginationDirection {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "f" => Self::Forwards,
            "b" => Self::Backwards,
            _ => Self::default(),
        })
    }
}

impl Display for PaginationDirection {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            PaginationDirection::Forwards => "f",
            PaginationDirection::Backwards => "b",
        })
    }
}

#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
pub struct PaginationParameters {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub from: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub to: Option<String>,
    #[serde_as(as = "Option<DisplayFromStr>")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dir: Option<PaginationDirection>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<usize>,
}
