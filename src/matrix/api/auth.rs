use std::{convert::TryInto, str::FromStr};

use crate::{
    matrix::{
        api::discovery::DiscoveryInformation,
        client::{Empty, MatrixClient, MatrixClientError},
        identifier::{MatrixId, UserId},
    },
    store::DeviceStore,
};
use isocountry::CountryCode;
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum UserIdentifier {
    #[serde(rename = "m.id.user")]
    UserMatrixId { user: String },
    #[serde(rename = "m.id.thirdparty")]
    ThirdPartyId { medium: String, address: String },
    #[serde(rename = "m.id.phone")]
    PhoneNumber { country: CountryCode, phone: String },
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum AuthenticationMode {
    #[serde(rename = "m.login.password")]
    Password {
        identifier: UserIdentifier,
        password: String,
    },
}

#[derive(Debug, Deserialize, Serialize)]
pub struct LoginRequest {
    #[serde(flatten)]
    pub auth: AuthenticationMode,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub device_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub initial_device_display_name: Option<String>,
}

#[serde_as]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LoginResponse {
    #[serde_as(as = "DisplayFromStr")]
    pub user_id: MatrixId,
    pub access_token: String,
    pub device_id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub well_known: Option<DiscoveryInformation>,
}

impl From<LoginResponse> for DeviceStore {
    fn from(response: LoginResponse) -> Self {
        Self {
            matrix_id: response.user_id.to_string(),
            access_token: response.access_token,
            device_id: response.device_id,
        }
    }
}

#[serde_as]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct WhoamiResponse {
    #[serde_as(as = "DisplayFromStr")]
    pub user_id: MatrixId,
}

impl MatrixClient {
    pub async fn whoami(&self) -> Result<MatrixId, MatrixClientError> {
        Ok(self
            .api_client
            .get::<Empty, WhoamiResponse>(
                self.homeserver_url("/_matrix/client/r0/account/whoami"),
                self.access_token().await?,
                None,
            )
            .await
            .map(|x| x.user_id)?)
    }

    pub async fn try_cache_auth(&mut self) -> Result<bool, MatrixClientError> {
        let mxid_string = self.user_id.to_string();
        if let Ok(Some(store)) = DeviceStore::by_matrix_id(&self.pool, mxid_string.as_str()).await {
            let user_id =
                UserId::from_str(store.matrix_id.as_str()).expect("Authed as a non-user somehow?");
            self.user_id = user_id.clone();

            if let Ok(mxid_resp) = &self.whoami().await {
                return Ok(user_id == mxid_resp.try_into().expect("Authed as non-user"));
            }
        }
        Ok(false)
    }

    pub async fn auth_request(
        &mut self,
        auth: AuthenticationMode,
        device_id: Option<String>,
    ) -> Result<(), MatrixClientError> {
        let response: LoginResponse = self
            .api_client
            .post(
                self.homeserver_url("/_matrix/client/r0/login"),
                None,
                Some(&LoginRequest {
                    auth,
                    device_id,
                    initial_device_display_name: Some(String::from("mewtrix (0.1.0)")),
                }),
            )
            .await?;

        log::info!("Authenticated as {}", response.user_id);

        let store: DeviceStore = response.clone().into();

        self.user_id =
            UserId::from_str(store.matrix_id.as_str()).expect("Authed as a non-user somehow?");

        if let Err(why) = store.upsert(&self.pool).await {
            log::error!("Store Write Failed: {:?}", why);
        }

        if let Some(well_known) = response.well_known {
            self.reconfigure(well_known);
        }

        Ok(())
    }

    pub async fn logout(&mut self) -> Result<(), MatrixClientError> {
        log::info!("Logging out");
        self.api_client
            .post::<Empty, Empty>(
                self.homeserver_url("/_matrix/client/r0/logout"),
                self.access_token().await?,
                None,
            )
            .await?;
        DeviceStore::delete_by_matrix_id(&self.pool, self.user_id.to_string().as_str()).await?;

        Ok(())
    }

    pub async fn logout_all(&mut self) -> Result<(), MatrixClientError> {
        log::info!("Logging out of all sessions");
        self.api_client
            .post::<Empty, Empty>(
                self.homeserver_url("/_matrix/client/r0/logout/all"),
                self.access_token().await?,
                None,
            )
            .await?;
        DeviceStore::delete_by_matrix_id(&self.pool, self.user_id.to_string().as_str()).await?;

        Ok(())
    }
}
