use serde::{Deserialize, Serialize};

use super::events::StateEvent;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum MembershipState {
    Invite,
    Join,
    Knock,
    Leave,
    Ban,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum JoinRules {
    Public,
    Knock,
    Invite,
    Private,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct State {
    pub events: Vec<StateEvent>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct JoinedRoomInformation {
    pub summary: RoomSummary,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct RoomSummary {
    #[serde(rename = "m.heroes", default)]
    pub heroes: Vec<String>,
    #[serde(rename = "m.joined_member_count")]
    pub joined_member_count: Option<u64>,
    #[serde(rename = "m.invited_member_count")]
    pub invited_member_count: Option<u64>,
}
