# Mewtrix!

Mewtrix (stylised `Mewtrix!`) is a work-in-progress CLI Matrix client for fun. Everything is implemented from spec from scratch.

## Usage

Clone the repo, run `cargo run`.

`RUST_LOG` environment variable can be used to select log level from any of (`off`, `error`, `warn`, `info`, `debug`, `trace`)

Execute `/help` to view available commands

## License

Licenced under [CNPLv6](https://gitlab.com/elise/mewtrix/-/blob/main/LICENSE.txt)
