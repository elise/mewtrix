-- Add migration script here
CREATE TABLE preference_store(
    matrix_id TEXT PRIMARY KEY NOT NULL,
    presence TEXT CHECK( presence IN ( 'offline', 'online', 'unavailable' ) ) NOT NULL
);  