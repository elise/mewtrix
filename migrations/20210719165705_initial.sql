-- Add migration script here
CREATE TABLE device_store(
    matrix_id TEXT PRIMARY KEY NOT NULL,
    access_token TEXT NOT NULL,
    device_id TEXT NOT NULL
);